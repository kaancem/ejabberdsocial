%%%----------------------------------------------------------------------
%%% File    : mod_persistent_sql.erl
%%% Author  : Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%% Created : 17 Nov 2016 by Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%%
%%%----------------------------------------------------------------------

-module(mod_persistent_sql).
-behaviour(mod_persistent).

-compile([{parse_transform, ejabberd_sql_pt}]).

%% API
-export([init/2, save_message/4, get_messages/6]).

-include("logger.hrl").
-include("ejabberd_sql_pt.hrl").
-include("jlib.hrl").

init(_Host, _Opts) ->
    ok.

save_message(Host, From, To, Packet) ->
    %?INFO_MSG("Send packet~n    from ~p ~n    to ~p~n    packet ~p.",
    %  [From, To, Packet]),
    XML = encode_xml(Packet),
    LFrom = From#jid.luser,
    LTo = To#jid.luser,
    Timestamp = unix_utcnow(),
    Query = ?SQL("insert into persistent(from_username, to_username, created_at, xml) values (%(LFrom)s, %(LTo)s, %(Timestamp)d, %(XML)s)"),
    ejabberd_sql:sql_query(Host, Query).

get_messages(Host, FromUser, ToUser, End, Limit, Offset) ->
	Query = ?SQL(
        "SELECT @(from_username)s, @(to_username)s, @(xml)s, @(created_at)d "
        "FROM persistent "
        "WHERE "
        "((from_username = %(FromUser)s AND to_username = %(ToUser)s) "
        "OR "
        "(from_username = %(ToUser)s AND to_username = %(FromUser)s)) "
        "AND created_at <= %(End)d "
        "LIMIT %(Limit)d OFFSET %(Offset)d"),

    case catch ejabberd_sql:sql_query(Host, Query) of
        {selected, List} ->
            {ok, [{From, To, Timestamp, decode_xml(Xml)} || {From, To, Xml, Timestamp} <- lists:keysort(4, List)]};
        Reason ->
            ?ERROR_MSG("failed to get messages from: ~s to: ~s reason: ~p", [FromUser, ToUser, Reason]),
            {error, {invalid_result, Reason}}
    end.


encode_xml(Packet) ->
    fxml:element_to_binary(Packet).


decode_xml(Xml) -> 
    fxml_stream:parse_element(Xml).


unix_utcnow() ->
    {Mega, Secs, _} = erlang:timestamp(),
    1000000 * Mega + Secs.
