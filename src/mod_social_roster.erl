%%%----------------------------------------------------------------------
%%% File    : mod_social_roster.erl
%%% Author  : Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%% Purpose : Social plugins for retrieving friends.
%%% Created : 17 Nov 2016 by Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%%----------------------------------------------------------------------

-module(mod_social_roster).
-author('kaanugurlu@scorpapp.com').

-behaviour(gen_mod).

-export([start/2, stop/1, get_roster/3, process_sm_iq/3, mod_opt_type/1, depends/2]).

-include("ejabberd.hrl").
-include("logger.hrl").

-include("jlib.hrl").
-include("mod_roster.hrl").


-callback init(binary(), gen_mod:opts()) -> any().
-callback get_roster(binary(), binary(), {binary(), binary()}) -> [{atom(), {jid(), [{binary(), binary()}]}}].


start(Host, Opts) ->
    Mod = gen_mod:db_mod(Host, Opts, ?MODULE),
    Mod:init(Host, Opts),

    IQDisc = gen_mod:get_opt(iqdisc, Opts, fun gen_iq_handler:check_type/1, one_queue),
    gen_iq_handler:add_iq_handler(ejabberd_sm, Host, ?NS_SOCIAL_ROSTER, ?MODULE, process_sm_iq, IQDisc).


stop(Host) ->
    gen_iq_handler:remove_iq_handler(ejabberd_sm, Host, ?NS_SOCIAL_ROSTER).


process_sm_iq(From, To, IQ) ->
    #iq{sub_el = SubEl, lang = Lang} = IQ,
    #jid{lserver = LServer} = From,
    case lists:member(LServer, ?MYHOSTS) of
      true -> process_local_iq(From, To, IQ);
      _ ->
      Txt = <<"The query is only allowed from local users">>,
      IQ#iq{type = error,
        sub_el = [SubEl, ?ERRT_ITEM_NOT_FOUND(Lang, Txt)]}
    end.


%% IQ query used for obtaining social rooster.
process_local_iq(#jid{luser = LUser, lserver = LServer} = FromJID, ToJID, #iq{type = Type, lang = Lang, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            Txt = <<"Value 'set' of 'type' attribute is not allowed">>,
            IQ#iq{type = error, sub_el = [SubEl, ?ERRT_NOT_ALLOWED(Lang, Txt)]};
        get ->
            Auth = elem_to_auth(SubEl),
            try 
                case get_roster(LUser, LServer, Auth) of
                    {ok, Roster} -> 
                        try
                            update_roster(LUser, LServer, [JID || {JID, _} <- Roster])
                        catch
                            EI -> ?ERROR_MSG("Error at updating roster: ~p", [EI])
                        end,
                        Items = [
                            #xmlel{
                                name = <<"item">>,
                                attrs = [
                                    {<<"subscription">>, <<"both">>},
                                    {<<"jid">>, jid:to_string(JID)}],
                                children = [
                                    #xmlel{
                                        name = AttrName,
                                        attrs = [],
                                        children = [{xmlcdata, AttrVal}]
                                    } || {AttrName, AttrVal} <- UserAttributes
                                ]}
                            || {JID, UserAttributes} <- Roster],
                        IQ#iq{type = result,
                            sub_el = [#xmlel{
                                name = <<"query">>,
                                attrs = [{<<"xmlns">>, ?NS_SOCIAL_ROSTER}],
                                children = Items}]};
                    {invalid, Reason} -> IQ#iq{
                        type = error,
                        sub_el = [SubEl, ?ERRT_NOT_ACCEPTABLE(Lang, Reason)]};
                    {unauthorized, Reason} -> IQ#iq{
                        type = error,
                        sub_el = [SubEl, ?ERRT_NOT_AUTHORIZED(Lang, Reason)]};
                    {error, Reason} -> IQ#iq{
                        type = error,
                        sub_el = [SubEl, ?ERR_INTERNAL_SERVER_ERROR]}
                end
            catch
              E -> ?ERROR_MSG("Error at getting social roster: ~p", [E]),
              IQ#iq{type = error,
                sub_el = [SubEl, ?ERR_INTERNAL_SERVER_ERROR]}
            end
    end.


elem_to_auth(QueryEl) ->
    AuthType = fxml:get_path_s(QueryEl, [{elem, <<"auth">>}, {attr, <<"type">>}]),
    AuthValue = fxml:get_path_s(QueryEl, [{elem, <<"auth">>}, cdata]),
    {AuthType, AuthValue}.


get_roster(LUser, LServer, Auth) ->
    Mod = gen_mod:db_mod(LServer, ?MODULE),
    Mod:get_roster(LUser, LServer, Auth).


% This function is used for saving to original roster for proper presence support.
update_roster(LUser, LServer, UserJIDs) ->
    % Extract and convert all jids to string format for proper comparsion.
    UserSJIDs = [jid:to_string(jid:tolower(UserJID)) || UserJID <- UserJIDs],
    Rosters = mod_roster:get_user_roster([], {LUser, LServer}),

    ExistingJIDs = ordsets:from_list([from_roster(Roster) || Roster <- Rosters]),
    MustJIDs = ordsets:from_list(UserSJIDs),

    AddJIDs = ordsets:to_list(ordsets:subtract(MustJIDs, ExistingJIDs)),
    DelJIDs = ordsets:to_list(ordsets:subtract(ExistingJIDs, MustJIDs)),

    F = fun () ->
        Adds = [{jid:tolower(jid:from_string(AddJID)), to_roster(LUser, LServer, AddJID)} || AddJID <- AddJIDs],
        [mod_roster:roster_subscribe_t(LUser, LServer, AddLJID, AddRoster) || {AddLJID, AddRoster} <- Adds],
        [mod_roster:del_roster_t(LUser, LServer, jid:tolower(jid:from_string(DelJID))) || DelJID <- DelJIDs],
        ok
    end,
    mod_roster:transaction(LServer, F),
    ok.


%% Returns a roster record
to_roster(LUser, LServer, SJID) ->
    R = #roster{
        us = {LUser, LServer},
        jid = jid:tolower(jid:from_string(SJID)), name = <<"">>,
        subscription = both,
        ask = none,
        askmessage = <<"">>,
        groups = []
    },
    R.


%% Returns jid string from roster
from_roster(#roster{jid = JID} = Roster) ->
    SJID = jid:to_string(jid:tolower(JID)),
    SJID.


depends(_Host, _Opts) ->
    [].

mod_opt_type(db_type) -> fun(T) -> ejabberd_config:v_db(?MODULE, T) end;
mod_opt_type(iqdisc) -> fun gen_iq_handler:check_type/1;
mod_opt_type(db_opts) -> fun (T) -> is_list(T) end;
mod_opt_type(_) -> [db_type, iqdisc, db_opts].
