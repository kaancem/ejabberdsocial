%%%----------------------------------------------------------------------
%%% File    : mod_persistent.erl
%%% Author  : Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%% Purpose : Persisting messages for client who don't know their history.
%%% Created : 17 Nov 2016 by Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%%----------------------------------------------------------------------

-module(mod_persistent).
-author('kaanugurlu@scorpapp.com').

-behaviour(gen_mod).

%% gen_mod api
-export([start/2,
        stop/1,
        mod_opt_type/1,
        depends/2]).


%% Handlers
-export([user_send_packet/4,
        process_persistent_iq/3]).


%% Private
-export([
        save_message/4,
        get_messages/6]).


-include("ejabberd.hrl").
-include("logger.hrl").

-include("jlib.hrl").
%% -include("mod_persistent.hrl").

%% Callbacks related to storage backend
-callback init(binary(), gen_mod:opts()) -> any().
-callback save_message(binary(), #jid{}, #jid{}, #xmlel{}) ->
    {atomic, any()}.
-callback get_messages(binary(), binary(), binary(), non_neg_integer(), non_neg_integer(), non_neg_integer()) ->
    {ok, list()} | {error, any()}.



start(Host, Opts) ->
    Mod = gen_mod:db_mod(Host, Opts, ?MODULE),
    Mod:init(Host, Opts),

    ejabberd_hooks:add(user_send_packet, Host, ?MODULE, user_send_packet, 50),

    IQDisc = gen_mod:get_opt(iqdisc, Opts, fun gen_iq_handler:check_type/1,
                             one_queue),
    gen_iq_handler:add_iq_handler(ejabberd_sm, Host, ?NS_PERSISTENT, ?MODULE, process_persistent_iq, IQDisc).


stop(Host) ->
    ejabberd_hooks:add(user_send_packet, Host, ?MODULE, user_send_packet, 50),
    gen_iq_handler:remove_iq_handler(ejabberd_sm, Host, ?NS_PERSISTENT).


%% Check if the packet must be save.
check_must_save(Mod, Packet=#xmlel{name = <<"message">>, attrs = Attrs}) ->
    Type = fxml:get_attr_s(<<"type">>, Attrs),
    check_must_save_message_type(Mod, Type) andalso
    check_must_save_message(Mod, Packet);
check_must_save(_, _) ->
    false.


%% Save ordinary message types.
check_must_save_message_type(_, <<"">>)          -> true;
check_must_save_message_type(_, <<"normal">>)    -> true;
check_must_save_message_type(_, <<"chat">>)      -> true;
check_must_save_message_type(_, _)               -> false.

%% Messages must have a body. 
check_must_save_message(_Mod, Packet) ->
    Body     = fxml:get_subtag(Packet, <<"body">>),
    %% Used in mod_offline
    Delay    = fxml:get_subtag(Packet, <<"delay">>),
    %% Message Processing Hints (XEP-0334)
    NoStore  = fxml:get_subtag(Packet, <<"no-store">>),
    check_must_save_message_children(Body, Delay, NoStore).


check_must_save_message_children(false, _, _)     -> false;  %% No body.
check_must_save_message_children(_, false, false) -> true;   %% Has body, not offline or no-store.
check_must_save_message_children(_, _, _)         -> false.  %% offline or no-store.


save_message(Host, From, To, Packet) ->
    Mod = gen_mod:db_mod(Host, ?MODULE),    
    Mod:save_message(Host, From, To, Packet).


get_messages(Host, FromUser, ToUser, End, Limit, Offset) ->
    Mod = gen_mod:db_mod(Host, ?MODULE),
    Mod:get_messages(Host, FromUser, ToUser, End, Limit, Offset).


%% Save sent user packet to persistent storage.
user_send_packet(Packet, _C2SState, From, To) ->
    case check_must_save(?MODULE, Packet) of 
        true ->
            save_message(From#jid.lserver, From, To, Packet),
            Packet;
        false -> Packet
    end.


process_persistent_iq(From, To, IQ) ->
    handle_persistent_iq(From, To, IQ).


handle_persistent_iq(From, To, IQ) ->
    handle_lookup_messages(From, To, IQ).


handle_lookup_messages(
        #jid{luser = FromUser, lserver=Host},
        #jid{luser = ToUser},
        IQ=#iq{sub_el = QueryEl}) ->

    QueryID = fxml:get_tag_attr_s(<<"queryid">>, QueryEl),

    % Extract query params
    End = case elem_to_end(QueryEl) of 
        {ok, undefined} -> unix_utcnow(); %% Default end time
        {ok, QueryEnd} when QueryEnd >= 0 -> QueryEnd;
        _ -> undefined
    end,

    Limit = case elem_to_limit(QueryEl) of 
        {ok, undefined} -> 20;  %% Default limit
        {ok, QueryLimit} when QueryLimit > 0 -> QueryLimit;
        _ -> undefined
    end,

    Offset = case elem_to_offset(QueryEl) of 
        {ok, undefined} -> 0; %% Default offset
        {ok, QueryOffset} when QueryOffset >= 0 -> QueryOffset;
        _ -> undefined
    end,

    if 
        End == undefined -> 
            prepare_lookup_iq_error(IQ, <<"Invalid end.">>);
        Limit == undefined -> 
            prepare_lookup_iq_error(IQ, <<"Invalid limit.">>);
        Offset == undefined -> 
            prepare_lookup_iq_error(IQ, <<"Invalid offset.">>);
        true ->
            case get_messages(Host, FromUser, ToUser, End, Limit, Offset) of
                {ok, MessageRows} ->
                    %% Generate packets from rows.
                    Packets = [message_row_to_xml(Host, MessageRow) || MessageRow <- MessageRows],
                    [?INFO_MSG("Packet at ~p", [fxml:element_to_binary(Packet)]) || Packet <- Packets],
                    prepare_lookup_iq_success(IQ, QueryID, Packets);
                {error, Reason} ->
                    ?INFO_MSG("Error: ~p", [Reason]),
                    prepare_lookup_iq_error(IQ, Reason)
            end
    end.


message_row_to_xml(Host, {From, To, Timestamp, Packet}) -> 
    DateTime = calendar:now_to_universal_time({Timestamp div 1000000, Timestamp rem 1000000, 0}),
    wrap_packet(Host, From, To, DateTime, Packet).


wrap_packet(Host, From, To, DateTime, Packet) ->
    FromBin = to_binary({From, Host}),
    ToBin = to_binary({To, Host}),
    #xmlel{
        name = <<"message">>,
        attrs = [
            {<<"from">>, FromBin},
            {<<"to">>, ToBin}],
        children = [
            delay(DateTime, FromBin),
            Packet]}.

delay(DateTime, FromBin) ->
    timestamp_to_xml(DateTime, utc, FromBin, <<>>).

timestamp_to_xml(DateTime, Timezone, FromBin, Desc) ->
    {T_string, Tz_string} = jlib:timestamp_to_iso(DateTime, Timezone),
    Text = [{xmlcdata, Desc}],
    #xmlel{name = <<"delay">>,
           attrs = [{<<"xmlns">>, ?NS_DELAY},
                    {<<"from">>, FromBin},
                    {<<"stamp">>, list_to_binary(binary_to_list(T_string) ++ binary_to_list(Tz_string))}],
           children = Text}.


to_binary(#jid{user = User, server = Server, resource = Resource}) ->
    to_binary({User, Server, Resource});
to_binary({User, Server}) ->
    to_binary({User, Server, <<>>});
to_binary({Node, Server, Resource}) ->
    S1 = case Node of
             <<>> ->
                 <<>>;
             _ ->
                 <<Node/binary, "@">>
         end,
    S2 = <<S1/binary, Server/binary>>,
    S3 = case Resource of
             <<>> ->
                 S2;
             _ ->
                 <<S2/binary, "/", Resource/binary>>
         end,
    S3.



elem_to_end(QueryEl) ->
    case fxml:get_path_s(QueryEl, [{elem, <<"end">>}, cdata]) of
        undefined -> {ok, undefined};
        <<>> -> {ok, undefined};
        Timestamp -> cast_query_arg_to_int(Timestamp)
    end.

elem_to_limit(QueryEl) ->
    case fxml:get_path_s(QueryEl, [{elem, <<"limit">>}, cdata]) of 
        undefined -> {ok, undefined};
        <<>> -> {ok, undefined};
        Timestamp -> cast_query_arg_to_int(Timestamp)
    end.


elem_to_offset(QueryEl) ->
    case fxml:get_path_s(QueryEl, [{elem, <<"offset">>}, cdata]) of
        undefined -> {ok, undefined};
        <<>> -> {ok, undefined};
        Timestamp -> cast_query_arg_to_int(Timestamp)
    end.


unix_utcnow() ->
    {Mega, Secs, _} = erlang:timestamp(),
    1000000 * Mega + Secs.


cast_query_arg_to_int(QueryArg) -> 
    try
        {ok, binary_to_integer(QueryArg)}
    catch error:badarg ->
        {error, undefined}
    end.


prepare_lookup_iq_success(IQ, QueryID, Packets) ->
    IQ#iq{type = result,
      sub_el =
          [#xmlel{name = <<"query">>,
              attrs =
              [{<<"xmlns">>, ?NS_PERSISTENT},
               {<<"queryid">>, QueryID},
               {<<"num_sent">>, integer_to_binary(length(Packets))}],
              children = Packets}]}.

prepare_lookup_iq_error(IQ = #iq{lang = Lang, sub_el = SubEl}, Reason) ->
    IQ#iq{type = error, sub_el = [SubEl, ?ERRT_NOT_ALLOWED(Lang, Reason)]}.


depends(_Host, _Opts) ->
    [].

mod_opt_type(db_type) -> fun(T) -> ejabberd_config:v_db(?MODULE, T) end;
mod_opt_type(iqdisc) -> fun gen_iq_handler:check_type/1;
mod_opt_type(_) -> [db_type, iqdisc].
