%%%----------------------------------------------------------------------
%%% File    : mod_social_roster_http.erl
%%% Author  : Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%% Purpose : Social plugins for retrieving friends using http.
%%% Created : 21 Dec 2016 by Kaan Uğurlu <kaanugurlu@scorpapp.com>
%%%----------------------------------------------------------------------

-module(mod_social_roster_http).
-author('kaanugurlu@scorpapp.com').

-behaviour(mod_social_roster).

-export([init/2, get_roster/3]).

-include("ejabberd.hrl").
-include("logger.hrl").

-include("jlib.hrl").


init(Host, Opts) ->
    inets:start(),
    ok.


get_roster(LUser, LServer, Auth) ->
	case get_users(LUser, Auth) of
		{ok, Users} -> {ok, [{user_id_to_jid(UserID, LServer), UserProfile} || {UserID, UserProfile} <- Users]};
		{unauthorized, Reason} -> {unauthorized, Reason};
		{invalid, Reason} -> {invalid, Reason};
		{error, Reason} -> {error, Reason}
	end.


%% Convert UserID to JID
user_id_to_jid(UserID, LServer) ->
	jid:make(UserID, LServer, <<"">>).


%% Returns a list of {UserID, UserProfile} tuples. UserProfile is a 2-tuple list.
get_users(LUser, {Type, Value}) ->
	case Type of
		<<"token">> -> get_users_with_token_auth(LUser, Value);
		_ -> {invalid, <<"Unsupported authorization type.">>}
	end.

get_users_with_token_auth(LUser, Token) ->
	case httpc:request(
		get, {"http://127.0.0.1:8000/api/v110/user/friends/",
				[{"User-Agent", "Scorp/1"}, {"Authorization", "Token " ++ binary_to_list(Token)}]},
				[], []) of
		{ok, {{Version, StatusCode, ReasonPhrase}, Headers, Body}} -> 
			case StatusCode of
				200 -> {ok, serialize_success_body(Body)};
				401 -> {unauthorized, serialize_unauthorized_body(Body)};
				_ -> {error, list_to_binary("Unknown status code recieved: " ++ integer_to_list(StatusCode))}
			end;
		{error, Reason} -> ?ERROR_MSG("Error at getting friends: ~p", [Reason]), {error, <<"Server Error">>}
	end.


serialize_success_body(Body) ->
	case jiffy:decode(Body) of
		{[{<<"users">>, UserArray}]} when is_list(UserArray) -> lists:filter(
			fun(I) -> I =/= undefined end, [serialize_user_json(UserJson) || {UserJson} <- UserArray]);
		_ -> [] 
	end.

serialize_user_json(UserJson) -> 
	case lists:keyfind(<<"user">>, 1, UserJson) of
		{<<"user">>, {InnerUser}} when is_list(InnerUser) -> 
			case lists:keyfind(<<"id">>, 1, InnerUser) of
				{<<"id">>, UserID} ->
					UserProfile = [],
					UserProfile2 = case lists:keyfind(<<"username">>, 1, InnerUser) of
						{<<"username">>, Username} -> [{<<"username">>, Username} | UserProfile];
						false -> UserProfile
					end,
					UserProfile3 = case {lists:keyfind(<<"first_name">>, 1, InnerUser), lists:keyfind(<<"last_name">>, 1, InnerUser)} of
						{{<<"first_name">>, <<"">>}, {<<"last_name">>, LastName}} -> UserProfile2;
						{{<<"first_name">>, FirstName}, {<<"last_name">>, <<"">>}} -> UserProfile2;
						{{<<"first_name">>, FirstName}, {<<"last_name">>, LastName}} -> [{<<"name">>, binary_to_list(FirstName) ++ " " ++ binary_to_list(LastName)} | UserProfile2];
						{false, _} -> UserProfile2;
						{_, false} -> UserProfile2
					end,
					{case UserID of
						UserID when is_integer(UserID) -> integer_to_binary(UserID);
						UserID when is_binary(UserID) -> UserID;
						_ -> UserID
					end, UserProfile3};
				false -> undefined
			end;
		_ -> undefined
	end.

serialize_unauthorized_body(Body) ->
	case jiffy:decode(Body) of 
		{[{<<"detail">>, Message}]} when is_binary(Message) -> Message;
		_ -> <<"Authorization failed.">>
	end.
