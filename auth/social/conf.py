"""
    This module contains the confifurations for social authentication.
    Modify the parameteres to configure.
"""
import os

authenticator_class = 'social.token.MysqlTokenAuthenticator'
args = ()
kwargs = {
    'connection_conf': {
        'db_host': os.environ['AUTH_MYSQL_HOST'],
        'db_user': os.environ['AUTH_MYSQL_USER'],
        'db_pass': os.environ['AUTH_MYSQL_PASSWORD'],
        'db_name': os.environ['AUTH_MYSQL_DATABASE'],
        'db_port': int(os.environ.get('AUTH_MYSQL_PORT', 3306)),
    },
    'table_conf': {
        'table_name': 'authtoken_token',
        'user_col': 'user_id',
        'pass_col': 'key',
    }
}
