import MySQLdb
from social.base import SocialExtAuthenticator


class MysqlTokenAuthenticator(SocialExtAuthenticator):
    """
        Mysql token authenticator.
        Uses the token authentication mechanism to authorize users.
    """
    def __init__(self, connection_conf, table_conf):
        super(MysqlTokenAuthenticator, self).__init__()
        self.db_host = connection_conf['db_host']
        self.db_user = connection_conf['db_user']
        self.db_pass = connection_conf['db_pass']
        self.db_name = connection_conf['db_name']
        self.db_port = connection_conf.get('db_port', 3306)

        table_name = table_conf['table_name']
        user_col = table_conf['user_col']
        pass_col = table_conf['pass_col']

        self.auth_sql = "SELECT 1 FROM `{table_name}` WHERE `{user_col}` = %s AND `{pass_col}` = %s ".format(
            table_name=table_name, user_col=user_col, pass_col=pass_col)
        self.isuser_sql = "SELECT 1 FROM `{table_name}` WHERE `{user_col}` = %s ".format(
            table_name=table_name, user_col=user_col)

        self.conn = None

    def initialize(self):
        """
            Initialize the database connection.
        """
        if self.conn is not None:
            self.conn.close()

        self.conn = MySQLdb.connect(self.db_host, self.db_user, self.db_pass, self.db_name, self.db_port)

        import atexit
        atexit.register(self.terminate)

    def terminate(self):
        """
            Terminate the database connection.
        """
        if self.conn is not None:
            self.conn.close()

    def auth(self, user, host, password):
        with self.conn as cursor:
            cursor.execute(self.auth_sql, [user, password])
            row = cursor.fetchone()
            return bool(row and row[0])

    def isuser(self, user, host):
        with self.conn as cursor:
            cursor.execute(self.isuser_sql, [user])
            row = cursor.fetchone()
            return bool(row and row[0])
