import sys
import struct


class EjabberdInputError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class SocialExtAuthenticator(object):
    """
        Abstract social autheticatior class.
        In social authentication only functionality must be to authenticate users
        given username and password. Operations like remove/create/setpass must be handled
        externally in the application.

        The implementer has to provide auth and isuser methods to define authentication behaviour.
    """
    def get_ejabberd_request(self):
        input_length = sys.stdin.read(2)
        if len(input_length) is not 2:
            raise EjabberdInputError('Wrong input from ejabberd!')

        size = struct.unpack('>h', input_length)[0]
        income = sys.stdin.read(size).split(':')
        return income

    def sent_ejabberd_response(self, response):
        token = struct.pack('>hh', 2, 1 if response else 0)
        sys.stdout.write(token)
        sys.stdout.flush()

    def auth(self, user, host, password):
        raise NotImplementedError

    def isuser(self, user, host):
        raise NotImplementedError

    def initialize():
        pass

    def terminate():
        pass

    def start(self):
        self.initialize()

        while True:
            try:
                ejab_request = self.get_ejabberd_request()
            except EOFError:
                break

            op_result = False
            try:
                if ejab_request[0] == "auth":
                    op_result = self.auth(ejab_request[1], ejab_request[2], ejab_request[3])
                elif ejab_request[0] == "isuser":
                    op_result = self.isuser(ejab_request[1], ejab_request[2])
            except Exception:
                pass

            self.sent_ejabberd_response(op_result)

        self.terminate()
