from social import conf


def import_cls(path):
    from importlib import import_module
    sep = path.rfind('.')
    _module = import_module(path[:sep])
    return getattr(_module, path[sep + 1:])


auth_cls = import_cls(conf.authenticator_class)

authenticator = auth_cls(*conf.args, **conf.kwargs)
authenticator.start()
